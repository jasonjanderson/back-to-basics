(function () {
	var content,
		modal,
		overlay,
		trigger;
	
	trigger = document.getElementById('modalTrigger');
	document.onreadystatechange = registerOnready;
	
	function registerOnready() {
		if (document.readyState === 'interactive') {
			trigger.onclick = handleClick;
		}
	}
	
	function handleClick(event) {
		showOverlay();
		showModal();
	}
	
	function showOverlay() {
		if (!overlay) {
			overlay = document.createElement('div');
			document.body.appendChild(overlay);
			overlay.classList.add('overlay');
		}
		
		// init overlay
		overlay.setAttribute('id', 'modal-overlay');
		overlay.classList.remove('overlay--hidden');
		
		// allow closing
		overlay.onclick = closeModal;
		document.body.onkeydown = handleKeyDown;
	}
	
	function showModal() {
		if (!modal) {
			modal = document.createElement('div');
			overlay.appendChild(modal);
			modal.classList.add('modal');
		}
		
		// init modal
		modal.setAttribute('id', 'modal');
		modal.classList.remove('modal--hidden');
		content = document.getElementById('modal-content');
		content.classList.add('modal__content--visible');
		modal.appendChild(content);
	}
	
	function closeModal(event) {
		overlay.classList.add('overlay--hidden');
	}
	
	function handleKeyDown(event) {
		if (event.keyCode === 27 && modalIsOpen()) {
			closeModal();
		}
	}
	
	function modalIsOpen() {
		return !overlay.classList.contains('overlay--hidden');
	}
})();